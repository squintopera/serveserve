
# this is just for testing the system
#
#
#

try:
    import take
    take.append_to_sys("site_pkgs")
except:
    import sys
    sys.path.append("/mmfs1/library/script_library/pipeline/libs/3rd_party/production/")

import rpyc
import time
import shutil

src = "P:/Users/rupertt/fivegig.mov"
dst = "P:/Users/rupertt/fivegig_copy.mov"


start = time.time()

copy_service = rpyc.connect("192.168.73.21", 18861)
copy_service.root.copy(src, dst)

print time.time() - start

start = time.time()
shutil.copy(src, dst)
print time.time() - start

####
##   path conversions:
#
#   O:\    =>    /mmfs1/okido/
#   P:\    =>    /mmfs1/projects/
#   Q:\    =>    /mmfs1/EyePresents/
#   L:\    =>    /mmfs1/library/
#