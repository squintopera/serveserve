
try:
    import take
    take.append_to_sys("site_pkgs")
except:
    import sys
    sys.path.append("/mmfs1/library/script_library/pipeline/libs/3rd_party/production/")

import rpyc
import logging
import shutil
import os
import clique

class ServeService(rpyc.Service):
    def on_connect(self):
        # code that runs when a connection is created
        # (to init the serivce, if needed)
        pass

    def on_disconnect(self):
        # code that runs when the connection has already closed
        # (to finalize the service, if needed)
        pass

    def cased_path(self, path):
        # try to match the path given to a real path that exists, since the upper/lower case may be wrong from windows
        if os.path.exists(path):
            return path
        else:
            splitPath = os.path.normpath(path).split(os.sep)
            resolvedPath = ""
            match = True
            for part in splitPath:
                if os.path.exists(resolvedPath + "/" + part) or match == False:
                    resolvedPath = resolvedPath + "/" + part
                else:
                    match = False
                    for actualpart in os.listdir(resolvedPath):
                        if actualpart.lower() == part.lower():
                            resolvedPath = resolvedPath + "/" + actualpart
                            match = True
                            break
                    if not match:
                        resolvedPath = resolvedPath + "/" + part

            return resolvedPath
    
    def cased_pathB(self, path):
        # try to match the path given to a real path that exists, since the upper/lower case may be wrong from windows
        if os.path.exists(path):
            return path
        else:
            splitPath = os.path.normpath(path).split(os.sep)
            resolvedPath = ""
            match = True
            for part in splitPath:
                if os.path.exists(resolvedPath + "/" + part) or match == False:
                    resolvedPath = resolvedPath + "/" + part
                elif '%04d' in part:
                    for root, dirs, files in os.walk(resolvedPath):
                        collection = clique.assemble(files)
                        for item in collection[0]:
                            file_name = item.format().split(' ')[0]
                            if part.lower() == file_name.lower():
                                resolvedPath = resolvedPath + "/" + part
                                break
                        break
                else:
                    match = False
                    for actualpart in os.listdir(resolvedPath):
                        if actualpart.lower() == part.lower():
                            resolvedPath = resolvedPath + "/" + actualpart
                            match = True
                            break
                    if not match:
                        resolvedPath = resolvedPath + "/" + part

            return resolvedPath

    def translatePathToLinux(self, windowsPath):
        windowsPath = windowsPath.replace("\\", "/")
        replaceList=[("O:/", "/mmfs1/okido/"), ("o:/", "/mmfs1/okido/"),
                     ("P:/", "/mmfs1/projects/"), ("p:/", "/mmfs1/projects/"),
                     ("Q:/", "/mmfs1/EyePresent/"), ("q:/", "/mmfs1/EyePresent/"),
                     ("L:/", "/mmfs1/library/"), ("l:/", "/mmfs1/library/"),
                     ]
        for item in replaceList:
            windowsPath = windowsPath.replace(*item)
        linuxPath = windowsPath
        return linuxPath

    def translatePathToWin(self, linuxPath):
        replaceList=[("//mmfs1/okido/", "O:/"), ("/mmfs1/okido/", "O:/"),
                     ("//mmfs1/projects/", "P:/"), ("/mmfs1/projects/", "P:/"),
                     ("//mmfs1/EyePresent/", "Q:/"), ("/mmfs1/EyePresent/", "Q:/"),
                     ("//mmfs1/library/", "L:/"), ("/mmfs1/library/", "L:/")
                     ]
        for item in replaceList:
            linuxPath = linuxPath.replace(*item)
        return linuxPath

    def exposed_checkConnection(self):
        return True

    def exposed_copy(self, src, dst):
        translated_src = self.translatePathToLinux(src)
        translated_dst = self.translatePathToLinux(dst)

        if translated_src.startswith("/mmfs1/") and translated_dst.startswith("/mmfs1/"):
            shutil.copy(self.cased_path(translated_src), self.cased_path(translated_dst))
        else:
            raise ValueError('src or dst of copy seem like invalid paths')
            
    def exposed_realPath(self, path):
        linux_path = self.translatePathToLinux(path)
        if linux_path.startswith('/mmfs1/'):
            linux_cased = self.cased_pathB(linux_path)
            real_cased_path = self.translatePathToWin(linux_cased)
            return real_cased_path
        else:
            return path


if __name__ == "__main__":
    logging.basicConfig()
    from rpyc.utils.server import ThreadedServer
    t = ThreadedServer(ServeService, port = 18861)
    t.start()
